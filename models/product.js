const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
	title:{type: String, required:true},
	barcode:{type: String, required:false},
	price:{type: Number, required:true},
	description:{type: String, required:false},
	category:{type: String, required:true},
	date:{type: Date, required:true},
	// userId:{type: mongoose.Schema.Types.ObjectId, ref:'User', required:true,},
},{versionKey: false});

module.exports = mongoose.model('Product',productSchema);