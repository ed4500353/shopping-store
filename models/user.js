const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	accountId:{type: String, required:true},
	password:{type: String, required:true},

},{versionKey: false});

module.exports = mongoose.model('User',userSchema);