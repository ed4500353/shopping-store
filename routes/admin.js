const express = require('express');
const router = express.Router();
const isAuth = require('../middleware/is-auth');
const {
    check
} = require('express-validator');

const adminController = require('../controllers/admin-controller');

router.get('/add-product', isAuth, adminController.getAddProduct);

router.post('/add-product', isAuth, [check('price').isNumeric().withMessage('價格限輸入數字'), check('title').trim().isLength({
    min: 1
}).withMessage('商品名稱不得為空')], adminController.postAddProduct);

router.get('/edit-product/:productId', isAuth, adminController.getEditProduct);

router.post('/edit-product', isAuth, [check('price').isNumeric().withMessage('價格限輸入數字'), check('title').trim().isLength({
    min: 1
}).withMessage('商品名稱不得為空')], adminController.postEditProduct);

router.post('/delete-product/:productId', isAuth, adminController.postDeleteProduct);

module.exports = router;