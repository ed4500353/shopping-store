var express = require('express');
var router = express.Router();
const isAuth = require('../middleware/is-auth');
const productController = require('../controllers/product-controller');
const {
    check
} = require('express-validator');

router.get('/', isAuth, productController.getSearch);

router.post('/', check('search').trim().isLength({
    min: 1
}).withMessage('不得輸入空白內容'), isAuth, productController.postSearch);

router.get('/product-list', isAuth, productController.getProduct);

router.get('/product/:productId', isAuth, productController.getProductDetail);

module.exports = router;