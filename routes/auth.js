const express = require('express');
const router = express.Router();
const authController = require('../controllers/auth-controller');
const isAuth = require('../middleware/is-auth');
const {
    check,
    body
} = require('express-validator');
const rateLimit = require('express-rate-limit');

const limiter = rateLimit({
    windowMs: 60 * 60 * 1000, // 限制時間
    max: 1000, // 限制請求數量
    message: 'Too many requests, please try again later!',
})

router.get('/signup', authController.getSignup);

router.post('/signup', [check('password').isLength({
        min: 6
    }).withMessage('密碼不得小於6碼'), body('rePassword')
    .custom((value, {
        req
    }) => {
        if (value !== req.body.password) {
            throw new Error('確認密碼與密碼不相符')
        }
        return true;
    })
], authController.postSignup);

router.get('/login', limiter, authController.getLogin);

router.post('/login', authController.postLogin);

router.post('/logout', authController.postLogout);

module.exports = router;