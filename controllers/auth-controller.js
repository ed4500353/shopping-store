const User = require('../models/user');
const bcrypy = require('bcryptjs');
const {
	validationResult
} = require('express-validator');


exports.getLogin = (req, res, next) => {
	if (req.session.isLoggedIn) {
		return res.redirect('/');
	}
	res.render('auth/login', {
		title: '登入',
		errorMessages: req.flash('error'),
	});
}

exports.postLogin = (req, res, next) => {
	const accountId = req.body.accountId;
	const password = req.body.password;

	User.findOne({
		accountId
	}).then((user) => {
		// 驗證帳號是否存在
		if (!user) {
			req.flash('error', '查無此用戶');
			return res.redirect('/login');
		}
		// 驗證密碼是否正確
		bcrypy.compare(password, user.password).then(result => {
			if (result == false) {
				req.flash('error', '密碼錯誤');
				return res.redirect('/login');
			}
			req.session.isLoggedIn = true;
			res.redirect('/');
		})
	})
}

// 登出
exports.postLogout = (req, res, next) => {
	req.session.destroy((err) => {
		console.log(err);
		res.redirect('/login');
	})
}

// 新增使用者
exports.getSignup = (req, res, next) => {
	res.render('auth/signup', {
		title: '新增使用者',
		errorMessages: req.flash('error'),
		oldInput: {
			accountId: '',
		},
	});
}

exports.postSignup = (req, res, next) => {
	const accountId = req.body.accountId;
	const password = req.body.password;

	const errors = validationResult(req);

	// 驗證後如有錯誤將會回傳錯誤訊息
	if (!errors.isEmpty()) {
		console.log(errors.array());
		return res.status(422).render('auth/signup', {
			title: '新增使用者',
			errorMessages: errors.array()[0].msg,
			oldInput: {
				accountId: accountId,
			},
		});
	}

	// 驗證帳號是否存在
	User.findOne({
		accountId
	}).then((user) => {
		if (user) {
			req.flash('error', '使用者已存在');
			return res.redirect('/signup');
		}
		// 將使用者密碼加密
		bcrypy.hash(password, 12).then(hashedPassword => {
			const user = new User({
				accountId,
				password: hashedPassword
			});
			user.save().then(() => {
				res.redirect('/login')
			})
		})
	})
}