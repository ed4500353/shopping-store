const Product = require('../models/product');
const moment = require('moment');
const {
	validationResult
} = require('express-validator');

// 新增產品
exports.getAddProduct = (req, res, next) => {
	res.render('add-product', {
		title: '新增產品',
		activeAddProduct: true,
		editMode: false,
		errorMessages: req.flash('error'),
		oldInput: {
			title: '',
			description: '',
			price: '',
			category: '',
			barcode: '',
		},
		moment,
	});
}

exports.postAddProduct = (req, res, next) => {
	const {
		title,
		description,
		price,
		category,
		barcode,
		date
	} = req.body;

	const errors = validationResult(req);

	// 驗證後如有錯誤將會回傳錯誤訊息
	if (!errors.isEmpty()) {
		console.log(errors.array());
		return res.status(422).render('add-product', {
			title: '新增產品',
			activeAddProduct: true,
			editMode: false,
			errorMessages: errors.array()[0].msg,
			oldInput: {
				title: title,
				description: description,
				price: price,
				category: category,
				barcode: barcode,
			},
		});
	}

	const product = new Product({
		title,
		price,
		description,
		category,
		barcode,
		date,
	});

	if (product.barcode) {

		Product.findOne({
			barcode: product.barcode
		}).then(result => {
			console.log(result);
			if (result) {
				req.flash('error', '此商品條碼已存在');
				return res.redirect('/admin/add-product');
			}

			product
				.save()
				.then(result => {
					return res.redirect('/');
				});

		})

	}
	product
		.save()
		.then(result => {
			return res.redirect('/');
		});


}

//編輯產品
exports.getEditProduct = (req, res, next) => {
	const productId = req.params.productId;
	Product.findById(productId).then((productDetail) => {
		res.render('add-product', {
			title: '修改產品',
			productDetail,
			editMode: true,
			errorMessages: req.flash('error'),
			oldInput: {
				title: '',
				description: '',
				price: '',
				category: '',
				barcode: '',
			},
			moment,
		});
	})
}

exports.postEditProduct = (req, res, next) => {
	const {
		title,
		description,
		price,
		category,
		barcode,
		date,
		productId
	} = req.body;

	const errors = validationResult(req);

	if (!errors.isEmpty()) {
		console.log(errors.array());
		return res.status(422).render('add-product', {
			title: '修改產品',
			productDetail: {
				_id: productId,
				title: '',
				price: '',
				description: '',
				category: '',
				barcode: '',
			},
			editMode: true,
			errorMessages: errors.array()[0].msg,
			oldInput: {
				title: title,
				description: description,
				category: category,
				barcode: barcode,
			},
		});
	}

	Product.findById(productId).then((product) => {
		product.title = title;
		product.description = description;
		product.price = price;
		product.category = category;
		product.barcode = barcode;
		product.date = date;

		product.save().then((result) => {
			res.redirect('/product/' + productId);
		})
	});
}

// 刪除產品
exports.postDeleteProduct = (req, res, next) => {
	const productId = req.params.productId;
	const search = req.body.search.trim();
	const searchCategory = req.body.searchCategory.trim();

	Product.findByIdAndDelete(productId).then(() => {
		if (searchCategory === '全部') {
			Product.find({
				title: {
					$regex: search
				}
			}).exec().then((result) => {
				res.render('product-list', {
					title: '產品列表',
					search,
					searchCategory,
					product: result,
					moment,
				});
			})
		} else {
			Product.find({
				category: searchCategory,
				title: {
					$regex: search
				}
			}).exec().then((result) => {
				res.render('product-list', {
					title: '產品列表',
					search,
					searchCategory,
					product: result,
					moment,
				});
			})
		}
	});
}