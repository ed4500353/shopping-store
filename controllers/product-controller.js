const Product = require('../models/product');
const moment = require('moment');
const {
	validationResult
} = require('express-validator');
let product = [];

// 搜尋產品
exports.getSearch = (req, res, next) => {
	product = [];
	res.render('search', {
		title: '商品查詢',
		activeIndex: true,
		errorMessages: req.flash('error'),
		oldInput: {
			searchCategory: '',
			btnradio: '',
		},
	});
}

exports.postSearch = async (req, res, next) => {
	search = req.body.search.trim();
	searchCategory = req.body.searchCategory.trim();
	btnradio = req.body.btnradio;

	console.log(searchCategory);

	const errors = validationResult(req);

	// 驗證後如有錯誤將會回傳錯誤訊息
	if (!errors.isEmpty()) {
		console.log(errors.array());
		return res.status(422).render('search', {
			title: '產品查詢',
			activeIndex: true,
			errorMessages: errors.array()[0].msg,
			oldInput: {
				searchCategory: searchCategory,
				btnradio: btnradio,
			},
		});
	}

	// 判斷搜尋的形式（產品名稱or條碼）
	if (btnradio === 'title') {
		// 判斷產品類別
		if (searchCategory === '全部') {
			product = await Product.find({
				title: {
					$regex: search
				}
			});
		} else {
			product = await Product.find({
				category: searchCategory,
				title: {
					$regex: search
				}
			}).exec();
		}
		res.redirect('/product-list');

	} else {
		// 判斷產品類別
		if (searchCategory === '全部') {
			product = await Product.find({
				barcode: {
					$regex: search
				}
			});
		} else {
			product = await Product.find({
				category: searchCategory,
				barcode: {
					$regex: search
				}
			}).exec();
		}
		res.redirect('/product-list');
	}
}

//產品列表
exports.getProduct = (req, res, next) => {
	res.render('product-list', {
		title: '產品列表',
		search,
		searchCategory,
		product: product.sort(function (a, b) {
			return a.title > b.title ? 1 : -1;
		}),
		moment,
	});
}

// 產品詳情
exports.getProductDetail = async (req, res, next) => {
	const productId = req.params.productId;
	const productDetail = await Product.findById(productId)
	res.render('product-detail', {
		title: productDetail.title,
		productDetail,
		moment,
	});
}